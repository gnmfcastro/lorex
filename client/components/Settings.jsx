Settings = React.createClass({
    render() {
        return (
            <div>
                <Profile ionModal={this.props.ionModal} setModalState={this.props.setModalState}/>
                <SettingsList ionModal={this.props.ionModal} />
            </div>
        )
    }
});

NotLoggedIn = React.createClass({
    login() {
        Meteor.loginWithFacebook({"data": [
            {
                "permission": "public_profile",
                "status": "granted"
            }
        ]},function(res){
           console.log(res)
        });
    },
    render() {
        return <a onClick={this.login}>Login</a>
    }
});

LoggedIn = React.createClass({
    logout() {
        Meteor.logout();
    },
    render() {
        return (
            <div>
                <a onClick={this.logout}>Logout</a>
            </div>
        )
    }
});

Profile = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData() {
        let handle = Meteor.subscribe("user");
        return {
            loading: !handle.ready(),
            user: Meteor.user(),
            userLoading: Meteor.loggingIn()
        }
    },
    getLoginStatus() {
        if (this.data.userLoading || !this.data.user) {return false;}
        if (this.data.user) {
            return true;
        }
        return false
    },
    render() {
        let loginStatus = this.getLoginStatus();
        let profilePic;
        if (loginStatus) {
            if(this.data.user.services) {
                profilePic = "http://graph.facebook.com/" + this.data.user.services.facebook.id + "/picture/?type=large"
            }
        }
        return (
            <div className="profile-wrapper">
                <div className="image-wrapper">
                    {loginStatus ? <img src= {profilePic} /> : <div></div>}
                </div>
                <div className="login-wrapper">
                    {loginStatus ? <LoggedIn ionModal={this.props.ionModal} /> : <NotLoggedIn ionModal={this.props.ionModal} setModalState={this.props.setModalState} />}
                </div>
            </div>
        )
    }
})

SettingsList = React.createClass({
    getDefaultProps() {
        return {
            settings: ["Setting 1", "Setting 2", "Setting 3"]
        }
    },
    render() {
        let list = this.props.settings.map((setting) => {
            return (
                <div onClick={this.props.ionModal.bind(null, setting)} className="item" key={setting}>
                    <h2><a>{setting}</a></h2>
                </div>
            )
        })
        return (
            <div className="list">
                {list}
            </div>
        )
    }
})

LoginForm = React.createClass({
    getInitialState(){
      return{
          user: "",
          pass: ""
      }
    },
    handleChange(input, e) {
        if (input == "user") {
            this.setState({
                user: e.target.value
            })
        };
        if (input == "pass") {
            this.setState({
                pass: e.target.value
            })
        }
    },
    render() {

        var user = this.state.user;
        var pass = this.state.pass;


        return (
            <div>
                <div className="list">
                    <label className="item item-input">
                        <span className="input-label">Username</span>
                        <input value={user} type="text" onChange={this.handleChange.bind(this, "user")} />
                    </label>
                    <label className="item item-input">
                        <span className="input-label">Password</span>
                        <input value={pass} type="password" onChange={this.handleChange.bind(this, "pass")} />
                    </label>
                </div>
                <div className="padding">
                    <button onClick={this.props.login.bind(null, this.state.user, this.state.pass)} className="button button-block button-positive">
                        Log in
                    </button>
                </div>
            </div>
        )
    }
})
