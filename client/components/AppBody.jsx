let Transition = React.addons.CSSTransitionGroup;

AppBody = React.createClass({
    getDefaultProps() {
        return {
            tabs: [{
                title: "My books",
                link: "/myBooks"
            }, {
                title: "X",
                link: "/"
            }, {
                title: "Settings",
                link: "/settings"
            }]
        }
    },
    getInitialState() {
        return {
            modal: false
        }
    },
    setModalState(status) {
        this.setState({
            modal: status
        })
    },
    ionModal(tab,content) {
        this.setState({
            modal: (
                <IonModal modalContent={content}>
                    <div className="h1 title">{tab}</div>
                    <button onClick={ () => this.setState({modal:false}) } className="button button-icon active">
                        <i className="icon ion-ios-close-empty"></i>
                    </button>
                </IonModal>
            )
        })
    },
    render() {

        let tabs = this.props.tabs.map((tab, i) => {
            return (
                <ReactRouter.Link className="tab-item" to={tab.link}>
                    <i className="icon ion-star"></i>
                    {tab.title}
                </ReactRouter.Link>
            )
        })
        return (
                <div className="ionic-body">
                    <div className="bar bar-header bar-light">
                    <h4 className="h1 title" to={"/"}>Lorex</h4>
                    <ReactRouter.Link className="button button-icon icon ion-heart" to={"/other"}></ReactRouter.Link>
                    </div>
                    <div className="view">
                        <div className="scroll-content ionic-scroll">
                            <div className="content overflow-scroll has-header">
                                <ReactRouter.RouteHandler setModalState={this.setModalState} ionModal={this.ionModal} />
                            </div>
                        </div>
                    </div>
                    <div className="tabs tabs-icon-top">
                        {tabs}
                    </div>
                </div>
        )
    }
});
