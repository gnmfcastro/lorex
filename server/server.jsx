if (MyData.find().count() === 0) {
    _.each(_.range(25), function() {
        MyData.insert({
            name: faker.name.findName(),
            image: faker.image.people() + "?" + Random.hexString(24),
            details: faker.lorem.sentence()
        })
    })
}

Meteor.startup(function(){

    ServiceConfiguration.configurations.remove({
        service: 'facebook'
    });

    ServiceConfiguration.configurations.insert({
        service: 'facebook',
        appId: Meteor.settings.public.facebook.consumerKey,
        secret: Meteor.settings.facebook.consumerSecret
    });

});

Meteor.publish("myData", function() {
    return MyData.find();
});

Meteor.publish("user", function () {
    return Meteor.users.find({_id: this.userId});
});
